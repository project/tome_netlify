ABOUT TOME NETLIFY
==================

Tome Netlify provides integrations between Tome and Netlify, giving you the
best static Drupal experience possible.

INSTALLATION
============

Tome Netlify depends on Tome beta 2 or higher.

USE
===

Tome Netlify provides the following features:

* Generation of the Netlify _redirects file. When a redirect is generated by
  Tome Static, this module will write an equivalent line to a file named
  "_redirects" in the root of your static export directory.
* Send Contact form submissions to Netlify. With the "Contact" module enabled,
  you can edit any Contact form and enable Netlify submission by checking the
  "Submit form to Netlify" checkbox. Support for Netlify's built in honeypot
  and reCAPTCHA features can also be enabled per form. If you just want this
  feature without the deploy UI, you can enable "tome
