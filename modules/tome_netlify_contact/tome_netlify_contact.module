<?php

/**
 * @file
 * Contains hook implementation for Tome Netlify Contact.
 */

use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Config\Entity\ThirdPartySettingsInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\contact\MessageInterface;

/**
 * Adds custom configuration to the contact edit form.
 *
 * @param array $form
 *   The form array.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state.
 */
function tome_netlify_contact_form_contact_form_edit_form_alter(array &$form, FormStateInterface $form_state) {
  $form_object = $form_state->getFormObject();
  if (!($form_object instanceof EntityFormInterface)) {
    return;
  }

  $entity = $form_object->getEntity();
  if ((!$entity instanceof ThirdPartySettingsInterface)) {
    return;
  }

  $form['tome_netlify_contact'] = [
    '#type' => 'fieldset',
    '#title' => t('Tome Netlify settings'),
    '#tree' => TRUE,
  ];

  $form['tome_netlify_contact']['use_netlify'] = [
    '#type' => 'checkbox',
    '#title' => t('Submit form to Netlify'),
    '#description' => t('When enabled, form submissions will be sent to Netlify.'),
    '#default_value' => $entity->getThirdPartySetting('tome_netlify_contact', 'use_netlify', FALSE),
  ];

  $form['tome_netlify_contact']['use_captcha'] = [
    '#type' => 'checkbox',
    '#title' => t('Use reCAPTCHA challenge'),
    '#description' => t('When enabled, a reCAPTCHA 2 challenge will appear at the bottom of the form.'),
    '#states' => [
      'visible' => [
        ':input[name="tome_netlify_contact[use_netlify]"]' => ['checked' => TRUE],
      ],
    ],
    '#default_value' => $entity->getThirdPartySetting('tome_netlify_contact', 'use_captcha', FALSE),
  ];

  $form['tome_netlify_contact']['use_honeypot'] = [
    '#type' => 'checkbox',
    '#title' => t('Use honeypot'),
    '#description' => t('When enabled, a honeypot field will be used to detect spam.'),
    '#states' => [
      'visible' => [
        ':input[name="tome_netlify_contact[use_netlify]"]' => ['checked' => TRUE],
      ],
    ],
    '#default_value' => $entity->getThirdPartySetting('tome_netlify_contact', 'use_honeypot', FALSE),
  ];

  $form['tome_netlify_contact']['usage_note'] = [
    '#type' => 'container',
    'note' => [
      '#markup' => '<em>' . t('Note that with Netlify enabled, the "Message" and "Auto-reply" settings will not be used. As an alternative, set the "Redirect path" setting to a custom confirmation page.') . '</em>',
    ],
    '#states' => [
      'visible' => [
        ':input[name="tome_netlify_contact[use_netlify]"]' => ['checked' => TRUE],
      ],
    ],
  ];

  $form['#entity_builders'][] = 'tome_netlify_contact_form_contact_form_edit_form_builder';
}

/**
 * Sets third party settings on contact forms for Tome Netlify.
 *
 * @param string $entity_type
 *   An entity type ID.
 * @param \Drupal\Core\Config\Entity\ThirdPartySettingsInterface $entity
 *   The entity being built.
 * @param array $form
 *   The form array.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state.
 */
function tome_netlify_contact_form_contact_form_edit_form_builder($entity_type, ThirdPartySettingsInterface $entity, array &$form, FormStateInterface $form_state) {
  if ($form_state->getValue('tome_netlify_contact')) {
    foreach ($form_state->getValue('tome_netlify_contact') as $key => $value) {
      $entity->setThirdPartySetting('tome_netlify_contact', $key, $value);
    }
  }
}

/**
 * Adds Netlify elements and attributes to the given form.
 *
 * @param array $form
 *   The form array.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state.
 */
function tome_netlify_contact_form_contact_message_form_alter(array &$form, FormStateInterface $form_state) {
  $form_object = $form_state->getFormObject();
  if (!($form_object instanceof EntityFormInterface)) {
    return;
  }

  $entity = $form_object->getEntity();
  if ((!$entity instanceof MessageInterface)) {
    return;
  }

  $contact_form = $entity->getContactForm();

  if (!$contact_form->getThirdPartySetting('tome_netlify_contact', 'use_netlify', FALSE)) {
    return;
  }

  if ($contact_form->getRedirectPath()) {
    $form['#action'] = $contact_form->getRedirectPath();
  }

  if (isset($form['actions']['preview'])) {
    $form['actions']['preview']['#access'] = FALSE;
  }

  if (isset($form['copy'])) {
    $form['copy']['#access'] = FALSE;
  }

  // Netlify doesn't like [] in input names.
  $form['#pre_render'][] = function ($element) {
    array_walk_recursive($element, function (&$item, $key) {
      if ($key === '#name') {
        $item = str_replace(['[', ']'], ['_', ''], $item);
      }
    });
    return $element;
  };

  $form['#attributes']['netlify'] = '';

  if ($contact_form->getThirdPartySetting('tome_netlify_contact', 'use_captcha', FALSE)) {
    $form['actions']['#weight'] = isset($form['actions']['#weight']) ? $form['actions']['#weight'] : 100;
    $form['tome_netlify_recaptcha'] = [
      '#markup' => '<div data-netlify-recaptcha></div>',
      '#weight' => $form['actions']['#weight'] - 1,
    ];
  }

  if ($contact_form->getThirdPartySetting('tome_netlify_contact', 'use_honeypot', FALSE)) {
    $form['#attributes']['netlify-honeypot'] = 'tome-netlify-honeypot';
    $form['tome_netlify_honeypot'] = [
      '#children' => '<input name="tome-netlify-honeypot" style="display:none !important" tabindex="-1" autocomplete="off" />',
    ];
  }
}
